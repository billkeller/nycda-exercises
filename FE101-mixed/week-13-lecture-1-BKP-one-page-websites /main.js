$(document).on('ready', function(){
	
	$(window).on('scroll', function(){
		var pageTop = $(window).scrollTop();
		console.log(pageTop);
		
		if(pageTop > 200 && $('.nav-wrapper').css('position') != 'fixed'){
			console.log('greater than 200');

			// hide .nav-wrapper
			$('.nav-wrapper').hide(function(){
				// after hide, set fixed position
				$('.nav-wrapper').css('position', 'fixed');
				// then slide it down
				$('.nav-wrapper').slideDown('1000');
			});
			
		} else if (pageTop < 200 ) {
			$('.nav-wrapper').css('position', 'initial');
		}
	});

	// on click of page anchors, scroll
	$('.js-page-about').on('click', function(e){
		e.preventDefault();
		$.scrollTo('#page-about', 800);
	})

	$('.js-page-contact').on('click', function(e){
		e.preventDefault();
		$.scrollTo('#page-contact', 800);
	})

	$('.js-page-more').on('click', function(e){
		e.preventDefault();
		$.scrollTo('#page-more', 800);
	})

});