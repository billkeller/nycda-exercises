// Dependency Injection. Pass an object TO the function,
// instead of creating it INSIDE the function

// BAD:
var Person = function(fname, lname) {
	this.fname = fname;
	this.lname = lname;
};

function logPerson(){
	var john = new Person('John', 'Doe');
	console.log(john);
}

logPerson();


// GOOD:
var Person = function(fname, lname) {
	this.fname = fname;
	this.lname = lname;
};

// logPerson is no longer dependent on the person
function logPerson(person){
	console.log(person);
};
// dependency injection
var john = new Person('John', 'Doe');
logPerson(john);