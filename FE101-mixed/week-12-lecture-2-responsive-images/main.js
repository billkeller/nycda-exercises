$(document).ready(function() {
	/* -----------------------------------------------------
		Responsive Images
		https://raw.githubusercontent.com/xoxco/breakpoints/master/breakpoints.js
	----------------------------------------------------- */

	var lgImg = $('img').data('src-lg');
	var smImg = $('img').data('src-sm');

	$('img').attr('src', smImg);

	$(window).setBreakpoints({
		distinct: true,
		breakpoints: [320,768]
	});

	$(window).bind('enterBreakpoint768', function(){
		$('img').attr('src', lgImg);
	});

	$(window).bind('exitBreakpoint768', function(){
		$('img').attr('src', smImg);
	});
	
	
});