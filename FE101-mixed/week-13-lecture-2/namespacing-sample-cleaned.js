var mySite = window.mySite || {};

mySite.js_flash = function(msg, type, delay){
  if(typeof(type)==='undefined') type='notice';
  if(typeof(delay)==='undefined') delay=10000;
  if(type == 'notice'){
    klass = "notice"
  }else{
    klass = "alert"
  }
  if($(".flash.notice").length == 0){
    $(".flash-wrapper").append("<div class='flash notice'/>")
  }
  $(".flash.notice").html(msg).addClass(klass).parent().addClass(klass).slideDown()
  setTimeout(function(){$(".flash.js").parent().fadeOut(3000)}, 10000)
  return true
}



mySite.maskLoad = function(){
  $('body').prepend('<div class="loading-mask"><div class="processing-message">Processing<br><br><i class="fa fa-spinner fa-spin"></i></div></div>')
  $(".processing-message").css('top', $(document).height()/3)
}

mySite.unmaskLoad = function(){
  $(".loading-mask").remove()
}



mySite.allOptionsComplete = function(){
  complete = false
  if(!cardChosen()){
    Reveal.slide( 1, 0, undefined );
    js_flash("Don't forget to choose a card.")
  } else if(!messageWritten()){
    Reveal.slide( 2, 0, undefined );
    js_flash("Don't forget to write a message.")
  } else if(!addressFormComplete()){
    js_flash("Make sure you're done with the address form before submitting your postcard.")
  }else{
    complete = true
  }
  return complete
}

mySite.cardChosen = function(){
  chosen = false
  $(".card").each( function(i,v){
    if($(v).hasClass('selected')){
      chosen = true
    }
  })
  return chosen
}

mySite.messageWritten = function(){
  written = false
  if($(".message").val() != ""){
    written = true
  }
  return written
}

mySite.addressFormComplete = function(){
  return $(".destination")[0].checkValidity()
}