var foo = function(){alert('hi');}
$(document).on('ready', foo);

$(document).on('ready', function(){

	var newColor = 'rgba(0,0,0,.2)';
	// change CSS of .box on click event of .foo
	$('.foo').on('click', function(){
		$('.box').css({
			'background-color': 'blue',
			color: newColor,
			fontSize: '40px'
		});
		callback();
	});

	// custom event
	$('.foo').on('userHasSignedUp', function(){
		console.log('A user has signed up!');
	});
	// trigger our custom event
	$('.foo').trigger('userHasSignedUp');



	// setup a callback function
	var callback = function(){
		$('.box-three').fadeIn('slow');
	}
	// run the callback function after .fadeOut completes
	$('.box-two').fadeOut('slow', callback)

	$(".my-element").animate({
	  opacity: 0.25,
	  width: '70%'
	}, 'slow' );

}); // Close document .ready