// http://www.woothemes.com/flexslider/ - API documentation
$(document).on('ready', function(){
	$('.flexslider').flexslider({
		animation: 'slide',
		controlNav: false,
		directionNav: false,
		slideshowSpeed: 2000
	});
});
