/*--------------------------------------
Local Scope
--------------------------------------*/
var showName = function(fname){
	console.log('My name is ' + fname);
};

showName('Steve');
showName('Marvin');
// console.log(fname); // returns undefined

/*--------------------------------------
Global Scope
 - accessed anywhere in your javascript
--------------------------------------*/
var x = 'global';
console.log(x); // not in a function body, so it's global

var showX = function(){
	console.log(x); // logs 'global';
}

showX();

/* leave off var keyword to create a var in the global scope */
var setGlobal = function(){
	y = 'global again';
}
setGlobal();
console.log(y); // logs 'global again'

/* All global vars are located on the window object */

var setGlobalonWindow = function(){
	window.buster = 'Buster';
}

setGlobalonWindow();

/*--------------------------------------
Precedence
 - The most local scope wins
--------------------------------------*/

var foo = function(){
	var g = 'local only';
	console.log(g);
}

var g = 'also global';
console.log(g); // logs 'also global'
foo(); // logs 'local only'


/*-------------------------------------------
Block Scope
 - in JS only a function block defines scope
---------------------------------------------*/
var inBlock = false;

for(var i = 0; i < 5; i++){
	var inBlock = true;
}

console.log(inBlock); // this will be true!

/*-------------------------------------------
Closure
 - anonymous self executing function
---------------------------------------------*/
(function(){
	var closure = 'closure here again';
	console.log(closure);
})();

// console.log(closure); // will return undefined



