var myNumber = 100;

// If the variable is less than 10
if (myNumber < 10) {

	// Alert that it is less than 10
	alert('Your variable is less than 10');

} else if (myNumber === 10) {
	// else, alert that it is greater than 10
	// and let the user know what the variable was
	alert('things are equal');
} else {
	alert(myNumber + ' Is greater than 10');
}

// var myString = 'foobar';

// if (myString === 'foo'){
	
// 	alert('good news, your string is equal');

// } else {

// 	alert('Sorry, your strings are not equal');

// }