// setup our global namespace
var game = game || {};
game.listeners = game.listeners || {};

game.listeners = {
	attachResetBtn: function(){
		$('.btn-reup').on('click', function(){
			game.currentHealth = 100;
			$('.player-health').html(game.currentHealth);
		});
		console.log('attachResetBtn is now attached');
	}
}

$(document).on('ready', function(){
	game.listeners.attachResetBtn();

	// store game data via data-attributes
	game.currentHealth = 100;
	
	$('.player').data('health', game.currentHealth);
	$('.player-health').html(game.currentHealth);

	game.attack = function(){
		var newHealth = game.currentHealth - 10;
		console.log(newHealth);
		game.currentHealth = newHealth;
		$('.player').data('health', game.currentHealth);
		$('.player-health').html(game.currentHealth);

		if(game.currentHealth === 0){
			$('.player-health').html('you are dead');
			// game.currentHealth = 100;
		}
	};

	console.log(game.currentHealth);


	

	// game.attack = function(){
	// 	var newHealth = game.currentHealth - 10;
	// 	console.log(newHealth);
	// 	$('.player').data('health', newHealth);
	// 	$('.player-health').html(newHealth);

	// 	game.currentHealth = newHealth;
	// 	console.log(game.currentHealth);

	// 	if (game.currentHealth === 0){
	// 		$('.player-health').html('You are dead');
	// 	}
	// };


}); // Close document .ready

// Samples from todays Quizz:
// var name = "Bill";

// var addNums = function(num){
// 	return num + 2 - 5;
// }

// for(var i = 1; i <= 10; i++){
// 	console.log(i);
// }



