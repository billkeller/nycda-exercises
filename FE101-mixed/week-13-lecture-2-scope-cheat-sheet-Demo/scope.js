/* ----------------------------------------------------------------------
	Local scope
	A variable that pertains ONLY to the function you are in
------------------------------------------------------------------------- */
var showName = function(fname){
	console.log(fname); // fname is local variable.
};

/*
scope is not defined until function is invoked
 everytime you invoke showName, a new local scope is created
*/
showName('Steve');
showName('Mark');
showName('Jimmy');
// console.log(fname); // fname is not defined




/* ----------------------------------------------------------------------
	Global scope
	if variable definition is OUTSIDE of function, it's a global variable
---------------------------------------------------------------------- */
var x = 'global';
var showGlobal = function(){
	console.log(x); // logs 'global'
};
console.log(x); // logs 'global'

/* leaving off the var keyword inside a function
 makes it a global variable */
var setGlobal = function(){
	y = 'y is GLOBAL';
	var z = 'z is local';
};
setGlobal();
console.log(y); // logs 'y is GLOBAL'
// consoe.log(z); // z is not defined







/* ----------------------------------------------------------------------
	Global scope (window object)
	All global variables are located on the window object
---------------------------------------------------------------------- */
var setGlobalOnWindow = function(){
	window.buster = 'buster is global';
};

setGlobalOnWindow();
console.log(buster); // logs 'buster is global'







/* ----------------------------------------------------------------------
	Precedence
	the most local var takes precedence
---------------------------------------------------------------------- */
var g = 'global';
var foo = function(){
	var g = 'local';
	console.log(g); //
};

foo(); // logs 'local'
console.log('global g: ' + g); // logs 'global';





/* ----------------------------------------------------------------------
Block Scope
	Scope is ONLY created in FUNCTION blocks
---------------------------------------------------------------------- */

var inBlock = false;
for (var i = 0; i < 5; i++){
	var inBlock = true;
}

console.log(inBlock); // logs 'true', because a for loop is NOT a function block








/* ----------------------------------------------------------------------
	Closure
	wrap in anonymous, self-executing function to create local scope
---------------------------------------------------------------------- */
(function(){
	var closure = 'closure';
	console.log(closure);
})();

// console.log(closure); // closure is not defined

