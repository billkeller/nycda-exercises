$(document).on('ready', function(){

	var modal = function(state, contentId){
		if(state === 'open'){
			var content = $('#' + contentId).html();
			$('.modal-content').empty();
			$('.modal-content').html(content);

			$('.modal-wrapper').fadeIn(function(){
				console.log(content);
				$('body').addClass('modal-on');
			});
		
		} else if (state === 'close'){
			$('.modal-wrapper').fadeOut(function(){
				$('body').removeClass('modal-on');
			});
		}
	}

	// invoke our function
	
	$('.js-modal-open').on('click', function(){
		modal('open', 'modal-content-disclaim');
	});

	$('.js-modal-close').on('click', function(){
		modal('close');
	});
});