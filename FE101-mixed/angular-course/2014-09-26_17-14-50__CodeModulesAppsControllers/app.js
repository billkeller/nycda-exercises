// MODULE
var myApp = angular.module('myApp', ['ngMessages', 'ngResource']); // takes an array of dependencies

// CONTROLLERS (these are the models)
// Angular keeps this bound with the view (DOM element that contains ng-controller='mainController')
// Angular.js doing dependency injection
// $scope = pass an object to my controller function
myApp.controller('mainController', ['$scope', '$timeout', '$log', '$filter', '$resource', '$http', function($scope, $timeout, $log, $filter, $resource, $http) {
	// $scope becomes the 'middle piece'.
	// The piece between the view and the controller
	
	$log.log($scope);
	$scope.name = 'Tony fubar';
	$scope.handle = '';
	$scope.lowercasehandle = function(){
		return $filter('lowercase')($scope.handle);
	}
	$scope.getName = $filter('uppercase')($scope.name);

	// $scope.getName();
	// console.log($log);
	$log.info('Some info');
	$log.debug('Debug info');

	// $log.log($resource);
	$log.info('scope: ', $scope);

	$timeout(function(){
		$scope.name = 'Everybody';
	}, 3000);

	// Digest loop, manual watcher.
	$scope.$watch('handle', function(newValue, oldValue){
		$log.info('changed!');
		$log.log('old: ' + oldValue);
		$log.log('new: ' + newValue);
	});

	// without the inner $scope.$apply()
	// this is outside of the Angular.js context
	// it didn't start a digest loop (or, you should be using the $timeout service)
	setTimeout(function(){
		$scope.$apply(function(){ // tell angular to run the normal digest loop
			$scope.handle = 'newTwitterFoo';
			$log.log('scope changed!' + $scope.handle);
		});
	}, 4000);

	// Common directives
	$scope.chars = 5;
	$scope.rules = [
		{rulename: 'Must be 5 characters'},
		{rulename: 'Must not be used elsewhere'},
		{rulename: 'Must be cool'}
	];

	$log.log($scope.rules);

	$scope.alertClick = function(){
		alert('hey now');
	};

	/* // NON Angular way
	var rulesRequest = new XMLHttpRequest();
	rulesRequest.onreadystatechange = function(){
		if(rulesRequest.readyState === 4 && rulesRequest.status === 200){
			$scope.rulesMore = JSON.parse(rulesRequest.responseText);
			$log.log($scope.rulesMore.immediate[0].name);
		}
	};

	rulesRequest.open('GET', 'http://klr.fm/members.json', true);
	rulesRequest.send();
	*/

	// Google spreadsheet to JSON:
	// https://coderwall.com/p/duapqq/use-a-google-spreadsheet-as-your-json-backend
	$http.get('https://spreadsheets.google.com/feeds/list/1OXEPOK0rBX8cFXAlWQWiz5PbhFOB0ccGonIuqgsYk7Q/od6/public/values?alt=json')
		.success(function(result){
			$scope.rules = result.feed.entry;
			// angular For loop
</script>			angular.forEach($scope.rules, function(value, i) {
				console.log($scope.rules[i].gsx$white);
			});
			// $log.log($scope.rules);
			
		}).error(function(data, status){
			$log.error(status);
		});


}]);


// var searchFoo = function($scope, fname, lname, foo, bar){
// 	return 'jane doe';
// }
// console.log(angular.injector().annotate(searchFoo));

// Directive:
// an instruction to Angular to manipulatae a piece of the DOM (addClass, hide, create, etc.)

