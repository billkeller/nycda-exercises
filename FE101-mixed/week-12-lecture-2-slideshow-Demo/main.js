$(document).on('ready', function(){
	// show first img by default
	$('img').eq(0).show();

	// setup some defaults (current slide number, total slide count)
	var slideNum = 0;
	var slideCount = $('img').length;
	
	var doSlides = function(direction){
		// hide this one
		$('.slideshow img').eq(slideNum).fadeOut();
		
		// if direction is next
		if (direction === 'next'){
			// show next one, by increasing by 1
			slideNum = slideNum + 1;

			if(slideNum >= slideCount){
				slideNum = 0;
			}	
		} else {
			if (slideNum <= 0){
				slideNum = slideCount -1;
			} else {
				slideNum = slideNum -1;
			}
		}

		console.log(direction + ' ' + slideNum);

		$('.slideshow img').eq(slideNum).fadeIn();

	};

	// on click of img
	$('.slideshow img, .js-next').on('click', function(){
		doSlides('next');	
	});

	// on click of prev button
	$('.js-prev').on('click', function(){
		doSlides('prev');	
	});

	$('.js-play').on('click', function(){
		autoPlay = setInterval(function(){
			doSlides('next');
		}, 2000);
	});

	$('.js-pause').on('click', function(){
		clearInterval(autoPlay);
	});

}); // end doc ready

