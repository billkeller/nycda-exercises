$(document).ready(function() {
	/* -----------------------------------------------------
		Responsive Images
		https://raw.githubusercontent.com/xoxco/breakpoints/master/breakpoints.js
	----------------------------------------------------- */
	
	var lgImg = $('.responsive-img').data('src-lg')
	var smImg = $('.responsive-img').data('src-sm')

	/* default to small image */
	$('.responsive-img').attr('src', smImg);

	$(window).setBreakpoints({
		distinct: true,
		breakpoints: [
			320,
			480,
			768
		]
	});

	$(window).bind('enterBreakpoint768', function(){
		$('.responsive-img').attr('src', lgImg);
	});

	$(window).bind('exitBreakpoint768', function(){
		console.log('not in 768 or above');
		$('.responsive-img').attr('src', smImg);
	});
	
});