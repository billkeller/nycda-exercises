var currentHealth = 100;

var attack = function(){
	// remove 10 from currentHealth
	var newHealth = currentHealth - 10;
	$('.player').data('health', newHealth);
	$('.player').text('Current Health: ' + newHealth);
	currentHealth = newHealth;
}

$(document).on('ready', function(){
	$('.player').data('health', currentHealth);
	$('.player').text('Current Health: ' + currentHealth);
});