var modal = function(state, contentId){
	if(state === 'open'){
		$('.modal-wrapper').fadeIn(function(){
			if (contentId){
				var content = $(contentId).html();
				$('#modal-content').html(content);
			} else {
				var content = $('#content-default').html();
				$('#modal-content').html(content);
			}
			$('body').addClass('modal-on');	
		});
		
	} else {
		$('.modal-wrapper').fadeOut(function(){
			$('body').removeClass('modal-on');
		});
	}
};

$(document).on('ready', function(){

	$('.js-modal-activate').on('click', function(){
		modal('open', '#content-login');
	});

	$('.js-modal-logout').on('click', function(){
		modal('open', '#content-logout');
	});

	$('.js-modal-regular').on('click', function(){
		modal('open');
	});

	// now attach a click event to our .js-modal-close
	$('.js-modal-close').on('click', function(){
		modal();
	});

});
