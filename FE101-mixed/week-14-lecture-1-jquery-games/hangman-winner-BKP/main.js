var HANG = HANG || {}

HANG.guesses = 0;

HANG.hangman = function(word){
	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alpha = alpha.split('');
	console.log(alpha);

	// Update our letter picker with each letter in our alphabet array
	for (var i = 0; i < alpha.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alpha[i] + '">' + alpha[i] + '</span>');
	}

	// turn word into array of letters
	var word = word.split('');
	console.log(word);
	// update game board with empty spaces for each letter
	for (var i = 0; i < word.length; i++){
		$('#js-word').append('<span data-letter="' + word[i] + '"></span>');
	}

	// Update guess count by word length plus 5
	HANG.guesses = word.length + 5;

	// attach click events to each letter
	$('#js-letter-picker').on('click', 'span', function(){
		HANG.guesses = HANG.guesses -1;
		console.log('guesses remaning: ' + HANG.guesses);

		var selectedLetter = $(this).data('letter');
		console.log('clicked: ' + selectedLetter);

		// update word with letter
		$('#js-word span[data-letter="' + selectedLetter + '"]').text(selectedLetter);

		var count = $('#js-word span[data-letter="' + selectedLetter + '"]').length;
		console.log(count);

		if (count > 0) {
			$(this).addClass('letter-found');
		} else {
			$(this).addClass('letter-not-found');
		}

		// winner
		var spans = $('#js-word span:empty').length;
		console.log(spans);
		if(spans === 0) {
			alert('We have a winner!');
			$('.js-reset').trigger('click');
		}

		// Loser
		if (HANG.guesses === 0){
			alert('Sorry, you\'re hung!');
			$('.js-reset').trigger('click');
		}
	});
};

$(document).on('ready', function(){
	// HANG.foo = function(){
	// 	$('#js-letter-picker').append('<span data-letter="foo">FOOO</span>');
	// }

	$('.js-reset').on('click', function(e){
		e.preventDefault();
		$('#js-word, #js-letter-picker').empty();
		HANG.hangman('newgame');
	});

	HANG.hangman('mood');


});
