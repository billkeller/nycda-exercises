var TTT = TTT || {}

TTT.resetGame = function(){
	$('td').removeClass('marked-o').removeClass('marked-x').data('marked', '');
	$('.player-o').removeClass('player-current');
	$('.player-x').addClass('player-current');
	// reset player X to true
	TTT.playerXturn = true;		
};

TTT.winner = {
	curMark: '',
	check: function(){
		var curMark = TTT.winner.curMark;
		var marked1 = $('td.sq1').data('marked');
		var marked2 = $('td.sq2').data('marked');
		var marked3 = $('td.sq3').data('marked');
		var marked4 = $('td.sq4').data('marked');
		var marked5 = $('td.sq5').data('marked');
		var marked6 = $('td.sq6').data('marked');
		var marked7 = $('td.sq7').data('marked');
		var marked8 = $('td.sq8').data('marked');
		var marked9 = $('td.sq9').data('marked');

		if (
			// across
			marked1 === curMark && marked2 === curMark && marked3 === curMark ||
			marked4 === curMark && marked5 === curMark && marked6 === curMark ||
			marked7 === curMark && marked8 === curMark && marked9 === curMark ||
			// Up & Down
			marked1 === curMark && marked4 === curMark && marked7 === curMark ||
			marked2 === curMark && marked5 === curMark && marked8 === curMark ||
			marked3 === curMark && marked6 === curMark && marked9 === curMark ||
			// diagonals
			marked1 === curMark && marked5 === curMark && marked9 === curMark ||
			marked3 === curMark && marked5 === curMark && marked7 === curMark
			){
			return true;
		}
	},
	xWins: 0,
	oWins: 0
};

$(document).on('ready', function(){
	
	TTT.playerXturn = true;
	$('.player-x').addClass('player-current');

	console.log(TTT.playerXturn);

	$('td').on('click', function(){
		
		// If this space is already taken
		if ($(this).hasClass('marked-x') || $(this).hasClass('marked-o')){
			alert('This space is taken');
			return false;
		}
		
		// If it's playerX's turn
		if (TTT.playerXturn === true){
			$(this).addClass('marked-x').data('marked', 'x');
			$('.player-o').addClass('player-current');
			$('.player-x').removeClass('player-current');
			TTT.playerXturn = false;
		
		} else {
			$(this).addClass('marked-o').data('marked', 'o');;
			$('.player-x').addClass('player-current');
			$('.player-o').removeClass('player-current');
			
			TTT.playerXturn = true;
		}

		// at end of click event, check for a win
		TTT.winner.curMark = $(this).data('marked');

		// If there's a winner
		if (TTT.winner.check()){
			// If X won
			if(TTT.winner.curMark === 'x'){
				// increase xWins
				TTT.winner.xWins = TTT.winner.xWins +1;
				// Update DOM
				$('.js-wins-x').text(TTT.winner.xWins);
			// Else, O has won
			} else {
				TTT.winner.oWins = TTT.winner.oWins +1;
				$('.js-wins-o').text(TTT.winner.oWins);
			}
			alert(TTT.winner.curMark + ' has won');
			TTT.resetGame();
		}

		for (var i = 0; i < $('td').length; i++){
			var curTd = $(this).indexOf;
			console.log(curTd);

			var justClicked = $(this).attr('class');
			console.log(justClicked);
		}
		
	}); // END TD click event

	// Reset Click event
	$('.js-reset').on('click', function(e){
		e.preventDefault();
		TTT.resetGame();	
	});

	
});
