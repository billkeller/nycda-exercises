$(document).on('ready', function(){

	function hangman(word) {
		var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

		for (i = 0; i < alpha.length; i++){
			$('#letter-picker').append('<span class="guess" data-letter="' + alpha[i] + '">' + alpha[i] + '</span>');
		}

		// turn word into an array of letters
		var wordSplit = word.split('');
		console.log("the word, as an array via .split: " + wordSplit);

		for (i = 0; i < wordSplit.length; i++){
			$('#word').append('<span class="letter" data-letter="' + wordSplit[i] + '"></span>')
		}

		$('.guess').on('click', function() {
			var selectedLetter = $(this).data('letter');

			// each letter in the word
			$('#word [data-letter=' + selectedLetter + ']').each(function() {
				$(this).text(selectedLetter);
			})

			var count = $('#word [data-letter=' + selectedLetter + ']').length;
			
			if (count > 0) {
				$(this).css('color', 'green');
			} else {
				$(this).css('color', 'red');
			}
			
			$(this).unbind('click');
		});
	}

    $('.js-begin').click(function() {
        $('#word, #letter-picker').empty();
        hangman('web');
    });

});