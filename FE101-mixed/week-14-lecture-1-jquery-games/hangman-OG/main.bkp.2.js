$(document).on('ready', function(){

	function hangman(word){
		// Setup our letter-picker
		var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

		// For each leter in alpha, update DOM
		for(i = 0; i < alpha.length; i++){
			// console.log(alpha[i]);

			// Output to our letter-picker
			$('#letter-picker').append(
				'<span class="guess js-guess" data-letter="' + alpha[i] + '">' + alpha[i] + '</span>'
			);
		}

		// Turn the letters of our word parameter into an array
		var word = word.split('');
		// console.log(word);

		// for each letter in word, update DOM with empty spaces
		for(i = 0; i < word.length; i++){
			$('#word').append('<span class="letter" data-letter="' + word[i] + '"> </span>');
		}

		// on click of a guess
		$('.js-guess').on('click', function(){
			// store our guesses data-letter in a var
			var selectedLetter = $(this).data('letter');

			// find the letter in our #word that matches our selectedLetter
			$('#word span[data-letter='+ selectedLetter +']').text(selectedLetter);

			// get a count of how many letters were found in #word
			var count = $('#word span[data-letter='+ selectedLetter +']').length;
			// alert(count);

			// highlight clicked letters
			if (count > 0){
				$(this).css('color', 'green');
			} else {
				$(this).css('color', 'red');
			}

		});

	}


hangman('fort');

});