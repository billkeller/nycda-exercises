$(document).on('ready', function(){

	function hangman(word){
		var alpha = ['a', 'b', 'c', 'd'];

		for(i = 0; i < alpha.length; i++){
			// console.log('alpha: ' + alpha[i] );
			// Update the DOM with each letter in the alpha array
			$('#letter-picker').append('<span class="guess" data-letter="' + alpha[i] + '">' + alpha[i] + '</span>');
		}

		// take the word provided and split it into an array
		var word = word.split("");
		console.log(word);

		for(i = 0; i < word.length; i++){
			$("#word").append('<span class="letter" data-letter="' + word[i] + '"> _ </span>');
		}

		$('.guess').on('click', function(){
			var selectedLetter = $(this).data('letter');
			// alert(selectedLetter);

			$('#word [data-letter='+ selectedLetter +']').text(selectedLetter);
			
			var count = $('#word [data-letter='+ selectedLetter +']').length
			// alert(count);
			if (count > 0){
				$(this).css('color', 'green');
			} else {
				$(this).css('color', 'red');
			}

			// alert('clicked');

			$(this).unbind('click');

		});

	}

	hangman('dabbb');

});