var HANG = HANG || {};

HANG.guesses = 0;

HANG.hang = function(word){
	$('.game-begin').hide();
	$('#hang-word').val('');

	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alphaArr = alpha.split('');
	var word = word.split('');
	
	for (var i = 0; i < alphaArr.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alphaArr[i] + '">' + alphaArr[i] + '</span>');
	}

	for (var j = 0; j < word.length; j++){
		$('#js-word').append('<span data-letter="' + word[j] + '"></span>');
	}

	HANG.guesses = word.length + 4;

	$('#js-letter-picker span').on('click', function(){
		if ($(this).hasClass('has-letter') || $(this).hasClass('has-not-letter')){
			return false;
		}

		var selectedLetter = $(this).data('letter');
		var count = $('#js-word span[data-letter="' + selectedLetter + '"]').length;
		console.log('letter clicked: ' + selectedLetter);
		console.log('letters found: ' + count);
		$('#js-word span[data-letter="' + selectedLetter + '"]').text(selectedLetter);

		if (count >= 1 ){
			$(this).addClass('has-letter');
		} else {
			$(this).addClass('has-not-letter');
			HANG.guesses--;
		}

		if (HANG.guesses === 0){
			alert('Out of guesses');
		}
		console.log('Guesses remaining: ' + HANG.guesses);

	});



};

HANG.reset = function(){

}

$(document).on('ready', function(){
	$('form.game-begin').on('submit', function(){
		var word = $('#hang-word').val();
		HANG.hang(word);
		return false;
	});
	
});
