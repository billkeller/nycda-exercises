var HANG = HANG || {};

HANG.guess = 0;

HANG.hang = function(word){
	$('.game-begin').hide();
	$('#hang-word').val('');

	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alphaArr = alpha.split('');
	console.log(alphaArr);
	for(var i = 0; i < alphaArr.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alphaArr[i] + '">' + alphaArr[i] + '</span>')
	}

	var wordToGuess = word.split('');
	console.log(wordToGuess);
	HANG.guess = wordToGuess.length + 5;
	for(var j = 0; j < wordToGuess.length; j++){
		$('#js-word').append('<span data-letter="'+ wordToGuess[j] +'"></span>');
	}

	$('#js-letter-picker > span').on('click', function(){

		var selectedLetter = $(this).data('letter');
		$('#js-word > span[data-letter="' + selectedLetter + '"]').text(selectedLetter);
		var count = $('#js-word > span[data-letter="' + selectedLetter + '"]').length;
		console.log(count);

		if (count === 0){
			HANG.guess = HANG.guess -1;
			console.log('guesses remaining: ' + HANG.guess);
		}

		if (HANG.guess === 0){
			alert('You Lost');
			HANG.reset();
		}

		var remaining = $('#js-word span:empty').length;
		if (remaining === 0){
			alert('You won!');
			HANG.reset();
		}
	});

};

HANG.reset = function(){
	HANG.guess = 0;
	$('#js-letter-picker').empty();
	$('#js-word').empty();
	$('.game-begin').show();
}

$(document).on('ready', function(){
	
	$('.js-play').on('click', function(){
		var word = $('#hang-word').val();
		HANG.hang(word);
	});

	// HANG.hang('buster');
});
