var HANG = HANG || {}

HANG.guesses = 0;

HANG.hangman = function(word){
	//var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alpha = alpha.split('');
	
	console.log(alpha);
	console.log('alpha length: ' + alpha.length);

	for(var i = 0; i < alpha.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alpha[i] + '">' + alpha[i] + '</span>');
	}

	// Setup word to guess
	var word = word;
	var word = word.split('');
	console.log(word);

	// iterate over each item in word array
	for (var i = 0; i < word.length; i++){
		$('#js-word').append('<span data-letter="' + word[i] + '"></span>')
	}

	HANG.guesses = word.length + 5;

	$('#js-letter-picker > span').on('click', function(){
		var selectedLetter = $(this).data('letter');
		var count = $('#js-word span[data-letter="' + selectedLetter + '"]').length;
		HANG.guesses = HANG.guesses - 1;

		console.group('letter-picker click event:')
			console.log('guesses remaining: ' + HANG.guesses);
			console.log('letter clicked: ' + selectedLetter);
			console.log('count in word: ' + count);
		console.groupEnd();

		//console.log('letter clicked: ' + selectedLetter + ' count in word: ' + count);
		$('#js-word span[data-letter="' + selectedLetter + '"]').text(selectedLetter);

		if (count > 0) {
			$(this).addClass('letter-found');
		} else {
			$(this).addClass('letter-not-found');
		}

		// winner
		var foo = $('#js-word span:empty').length;
		console.log('empty spans: ' + foo);

		if (foo === 0){
			alert("You've won");
			HANG.restart();
			// NEEDED TO STOP EVERTHING
			// with return false
			return false;
		}

		if (HANG.guesses === 0) {
			alert("You've been hanged");
			HANG.restart();
			// NEEDED TO STOP EVERTHING
			// with return false
			return false;
		}

	});
};

HANG.restart = function(){
	// alert('restart me');
	HANG.guesses = 0;
	$('#js-word, #js-letter-picker').empty();
	// manually restart the game
	HANG.hangman('nycda');
}

// HANG.foo = function(){
// 	$('#js-letter-picker').append('<span>$$</span>')
// };

$(document).on('ready', function(){
	HANG.hangman('buster');
});