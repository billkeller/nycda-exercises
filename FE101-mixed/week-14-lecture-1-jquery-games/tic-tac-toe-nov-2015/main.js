var TTT = TTT || {};

TTT.playerXsTurn = true;

TTT.game = TTT.game || {
	play: function(context){
		// only proceed IF this td is not already taken
		console.log('context: ', context);
		if(context.hasClass('marked-x') || context.hasClass('marked-o')){
			alert('Space Taken');
			return false;
		}

		// click on a square adds an X if playerXsTurn is true
		if(TTT.playerXsTurn){
			context.addClass('marked-x').data('marked', 'x');
			// x just went, so playerXsTurn should now be set to false
			TTT.playerXsTurn = false;
		// Else, add an O
		} else {
			context.addClass('marked-o').data('marked', 'o');
			// O just wen, so plyerXsTurn should now be true
			TTT.playerXsTurn = true;
		}

		// Check for a winner, based on data-marked
		var curMark = context.data('marked');
		
		
		if(TTT.game.check(curMark)){
			alert('we have a winner');
		}
	},

	reset: function(){
		TTT.playerXsTurn = true;
		$('td').removeClass('marked-o marked-x');
	},

	check: function(curMark){
		// alert(curMark);
		var marked1 = $('td.sq1').data('marked');
		var marked2 = $('td.sq2').data('marked');
		var marked3 = $('td.sq3').data('marked');
		var marked4 = $('td.sq4').data('marked');
		var marked5 = $('td.sq5').data('marked');
		var marked6 = $('td.sq6').data('marked');
		var marked7 = $('td.sq7').data('marked');
		var marked8 = $('td.sq8').data('marked');
		var marked9 = $('td.sq9').data('marked');

		if (
			// across
			marked1 === curMark && marked2 === curMark && marked3 === curMark

		){
			return true;
			// alert('we have a winner');
		}
	}
};


$(document).on('ready', function(){

	// var playerXsTurn = true;

	$('td').on('click', function(){
		var context = $(this);
		TTT.game.play(context);	

	});

});
