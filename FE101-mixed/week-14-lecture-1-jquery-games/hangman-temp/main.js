var HANG = HANG || {};

HANG.hangman = function(word){
	// display the full alphabet
	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alpha = alpha.split('');
	console.log(alpha);

	for (var i = 0; i < alpha.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alpha[i] + '">' + alpha[i]+ '</span>')
	}

	var word = word;
	var word = word.split('');
	for (var i = 0; i < word.length; i++){
		$('#js-word').append('<span data-letter="' + word[i] + '"></span>')
	}

	$('#js-letter-picker span').on('click', function(){
		var selectedLetter = $(this).data('letter');
		// alert(selectedLetter);

		// select the SAME element in our word, above
		$('#js-word span[data-letter="' + selectedLetter + '"]').text(selectedLetter);

		var count = $('#js-word span[data-letter="' + selectedLetter + '"]').length;

		if(count > 0){
			$(this).addClass('letter-found');
		} else {
			$(this).addClass('letter-not-found');
		}
	});


}

$(document).on('ready', function(){
	HANG.hangman('buster');
});