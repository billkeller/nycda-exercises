$(document).ready(function() {
	
	var playerXsTurn = true;
	$('.player-x').addClass('current-player');

	// Clicking on a square (td) adds an X
	// if its playerXsTurn is true, else we'll
	// add an O
	$('td').on('click', function(){
		var $this = $(this);
		// alert('clicked');

		// Make sure that the TD is not already
		// occupied with an X OR an O
		if($(this).hasClass('marked-x') || $(this).hasClass('marked-o')){
			alert('Space taken');
			return false;
		}

		// If playerXsTurn is true
		if(playerXsTurn){
			$('.player-x').removeClass('current-player');
			$('.player-o').addClass('current-player');
			$(this).addClass('marked-x');
			playerXsTurn = false;

		} else {
			$('.player-x').addClass('current-player');
			$('.player-o').removeClass('current-player');
			$(this).addClass('marked-o');
			playerXsTurn = true;
		}

	});

	$('.js-new-game').on('click', function(){
		// remove ALL X's and O's from the board
		$('td').removeClass('marked-x marked-o');
		$('.player-x, .player-o').removeClass('current-player');
		$('.player-x').addClass('current-player');
		var playerXsTurn = true;
	});


});