$(document).ready(function() {
	
	// default to player X
	var playerXsTurn = true;

	// Clicking a square alternately adds an X or O
	$("td").on("click", function() {
		
		// Don't take your turn if this square already has an X or an O
		if($(this).hasClass("marked-x") || $(this).hasClass("marked-o")) {
			alert('Square taken');
			return false;
		}

		// If it's player Xs Turn
		if(playerXsTurn) {
			$(this).addClass("marked-x");
			// playerX used their turn, set playerXsTurn to false
			playerXsTurn = false;
			// Update player
			$(".player-x").removeClass("current-player");
			$(".player-o").addClass("current-player");
		
		// Else, it's player Os Turn
		} else {
		
			$(this).addClass("marked-o");
			// playerO used their turn, set playerXsTurn to true
			playerXsTurn = true;

			$(".player-x").addClass("current-player");
			$(".player-o").removeClass("current-player");
		}
	});

	// New game button clears the board
	$(".js-new-game").on('click', function() {
		$("td").removeClass("marked-x marked-o");
		playerXsTurn = true;
		$(".player-x").addClass("current-player");
		$(".player-o").removeClass("current-player");
	});


});