var HANG = HANG || {};

HANG.hangman = function(word){
	// setup our letter-picker
	// create the alphabet
	var alpha = 'abcdefghijklmnopqrstuvwxyz';
	var alpha = alpha.split('')
	console.log(alpha);
	console.log(alpha.length);

	// Fill out our letter picker with the alphabet;
	for(var i = 0; i < alpha.length; i++){
		$('#js-letter-picker').append('<span data-letter="' + alpha[i] + '">' + alpha[i] + '</span>');
	}

	// Put in an empty span for each letter in the word we're trying to guess
	var word = word.split('');
	console.log(word);

	HANG.guesses = word.length + 5;

	for(var i = 0; i < word.length; i++){
		$('#js-word').append('<span data-letter="' + word[i] + '"></span>');
	}

	// Add click event to each span in #js-letter-picker
	$('#js-letter-picker span').on('click', function(){
		var letterSelected = $(this).data('letter');
		console.log(letterSelected);

		// Check our #js-word for letterSelected
		$('#js-word span[data-letter="' +letterSelected + '"]').text(letterSelected);
		
		// Highlight letter clicked in our letter picker
		var count = $('#js-word span[data-letter="' +letterSelected + '"]').length;
		console.log('Count: ' + count);

		if(count >= 1){
			$(this).addClass('has-letter');
		} else {
			$(this).addClass('has-not-letter');
			HANG.guesses = HANG.guesses -1;
			// alert('you have ' + HANG.guesses + ' guess left.');
			$('#js-guesses').text('Guesses remaining: ' + HANG.guesses);

			if (HANG.guesses === 0) {
				alert("You've been hanged.");
				HANG.resetGame();
				return false;
			}
		}

		var lettersRemaining = $('#js-word span:empty').length;
		if (lettersRemaining === 0){
			alert("you're a winner");
			HANG.resetGame();
		}

	});
};

HANG.guesses = 0;

HANG.resetGame = function(){
	// empty our divs
	$('#js-word, #js-letter-picker, #js-guesses').empty();
	// HANG.hangman('chief');
}

$(document).on('ready', function(){
	// HANG.hangman('fooseball');

	$('#js-hangman-word').on('submit', function(){
		HANG.resetGame();
		var word = $('#js-hangman-word input').val();
		HANG.hangman(word);
		$('#js-hangman-word input').val('');
		return false;
	});
});

