$(document).ready(function() {
	/* -----------------------------------------------------
		Responsive Images
		https://raw.githubusercontent.com/xoxco/breakpoints/master/breakpoints.js
	----------------------------------------------------- */
	
	var smImg = $('.responsive-img').data('sm-src');
	var lgImg = $('.responsive-img').data('lrg-src');
	$('.responsive-img').attr('src', smImg);

	$(window).setBreakpoints({
	    distinct: true, 
	    breakpoints: [
	        320,
	        480,
	        768,
	        1024
	    ] 
	});     

	$(window).bind('enterBreakpoint768',function() {
		$('.responsive-img').attr('src', lgImg);
	});
	
});