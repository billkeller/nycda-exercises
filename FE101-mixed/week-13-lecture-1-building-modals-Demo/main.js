$(document).on('ready', function(){

	var modal = function(state){
		if (state === 'open'){
			$('.modal-wrapper').fadeIn(function(){
				$('body').addClass('modal-on');
			});
		} else {
			$('.modal-wrapper').fadeOut(function(){
				$('body').removeClass('modal-on');
			});	
		}
	}

	$('.js-modal-open').on('click', function(){
		modal('open');
	});

	$('.js-modal-close').on('click', function(){
		modal('close');
	});

}); // Closes on.ready