$(document).on('ready', function(){

	$('.js-menu').on('click', function(){
		$('.nav-main').slideToggle();
	});

	$(window).on('resize', function(){
		var w = $(window).width();
		
		if (w > 420 && $('.nav-main').is(':hidden')){
			$('.nav-main').removeAttr('style');
		}
	});

}); // Closes doc.ready