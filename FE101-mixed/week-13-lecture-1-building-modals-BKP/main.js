$(document).on('ready', function(){
	var modalWrap = $('.modal-wrapper');

	$('.js-modal-activate').on('click', function(e){
		e.preventDefault();
		modalWrap.fadeIn(function(){
			$('body').addClass('modal-on');
		});
	});

	$('.js-modal-close').on('click', function(){
		modalWrap.fadeOut();
	});
});