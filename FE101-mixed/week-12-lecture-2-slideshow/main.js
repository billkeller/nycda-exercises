$(document).on('ready', function(){

	// Show first slide
	$('.slideshow img').eq(0).show();
	// setup current slide num
	var slideNum = 0;
	var slideCount = $('.slideshow img').length;

	var slideClick = function(direction){
		// hide the current slide
		$('.slideshow img').eq(slideNum).fadeOut();
		
		if(direction === 'next'){
			// increase slideNum by 1
			slideNum = slideNum + 1;
			if(slideNum === slideCount){
				slideNum = 0;
			}
		} else {
			if(slideNum === 0){
				slideNum = slideCount -1;
			} else {
				slideNum = slideNum -1;
			}

			// slideNum = slideNum -1;
		}
		
		console.log(direction + ' ' + slideNum);
		// show the next slide
		$('.slideshow img').eq(slideNum).fadeIn();
	};

	$('.slideshow img').on('click', function(){
		slideClick('next');
	});

	$('.js-prev').on('click', function(){
		slideClick('prev');
	});

	$('.js-next').on('click', function(){
		slideClick('next');
	});

	$('.js-play').on('click', function(){
		startSlides = setInterval(function(){
			slideClick('next');
		}, 1000);
	});

	$('.js-pause').on('click', function(){
		clearInterval(startSlides);
	});
	
	// console.log(startSlides);

	// clearInterval(startSlides);






}); // Closes doc.ready