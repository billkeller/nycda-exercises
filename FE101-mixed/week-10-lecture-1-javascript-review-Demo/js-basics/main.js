/* Kicking it old school */
function change(){
	document.getElementById('foo').innerHTML = 'Change Again';
}

function addNumbers(){
  number1 = document.getElementById('number1').value;
  number2 = document.getElementById('number2').value;
  document.getElementById('result').innerHTML = (parseInt(number1) + parseInt(number2));
};

/* Basic Data Types */
var fname = 'Steve'; // String
var number = 5; // Number
var isCodingCool = true; // Boolean
var myArray = ['snoopy', 'garfield']; // Array
var myArray2 = [1, 500, 'train', 'plane']; // Array


/* Multi Dimensional Arrays */
var bigCats = ['tigers', 'panthers'];
var bigDogs = ['pit bulls', 'Labs'];
var animals = [bigCats, bigDogs];
console.log("animals: " + animals[0][1]);


// If Statement
// var num1 = 2000;
// if (num1 > 100) {
// 	alert("Alwayyyyyssss");
// } else {
// 	alert('neverrrr');
// }


// function someName(num1, num2){
// 	// console.log('Inside someName function ' + num1);
// 	return num1 + 10 + num2;
// };
// var foo = someName(40,5);
// console.log('the value of someName func: ' + foo);

// function alertName(person, person2) {
// 	alert(person);
// 	alert(person2);
// }

// alertName('steve', 'mark', 'james', 'marvin');

// IF Else If
// var myString = "food";

// if (myString === "fort") {
// 	alert("found the " + myString);
// } else if (myString === "food") {
// 	alert("Found the food");
// } else {
// 	alert("finally");
// }

// var door = "laptop";

// if (door === "computer"){
// 	alert('you won a computer');
// } else if (door === 'laptop'){
// 	alert('you won a laptop');
// } else {
// 	alert('you won something else');
// }


