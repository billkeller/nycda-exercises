/* If Statement */
// var num1 = 2000;
// if (num1 > 100) {
// 	alert("Alwayyyyyssss");
// }

/* If Else Statement */
// var num1 = 2000;
// if (num1 > 100) {
// 	alert("Alwayyyyyssss");
// } else {
// 	alert('neverrrr');
// }

/* Exercises */
// var myNumber = 4;
// if (myNumber > 10) {
// 	alert('greater than 10');
// } else {
// 	alert(myNumber + ' is greater than 10');
// }

/* If the variable is less than 10 */
// if (myNumber < 10) {
// 	// Alert that it is less than 10
// 	alert('Your variable is less than 10');
// } else if (myNumber === 10) {
// 	// else, alert that it is greater than 10
// 	// and let the user know what the variable was
// 	alert('things are equal');
// } else {
// 	alert(myNumber + ' Is greater than 10');
// }

/* IF Else Statement */
// var myString = "food";

// if (myString === "fort") {
// 	alert("found the " + myString);
// } else if (myString === "food") {
// 	alert("Found the food");
// } else {
// 	alert("finally");
// }

/* Declaring Functions  */
// function someName(num1, num2){
// 	// console.log('Inside someName function ' + num1);
// 	return num1 + 10 + num2;
// };
// var foo = someName(40,5);
// console.log('the value of someName func: ' + foo);

/* Another Simple Function Example */
// function alertName(person, person2) {
// 	alert(person);
// 	alert(person2);
// }

// alertName('steve', 'mark', 'james', 'marvin');

/* Exercise - Door */

// function openDoor(door){
// 	if (door === "red"){
// 		alert("you win a prize of GOLD");
// 	} else if (door === "blue") {
// 		alert("You win a prize of BRONZE");
// 	} else {
// 		alert("No prize for you");
// 	}
// }

// openDoor("blue");


/* Accessing Array Positions with indexof() */
// var myArr = ["garfield", "snoopy", "popeye"];
// var snoopyPosition = myArr.indexOf("snoopy");
// console.log(myArr[snoopyPosition]);

// console.log(snoopyPosition);


/* Basic for Loops */
// for (var i = 0; i <= 10; i++){
// 	console.log(i);
// }

// var beers = ["Lagunitas", "Peak"]
// for(var i = 0; i< beers.length; i++){
//   console.log(beers[i])
// }

/* Exercise - for loops */
// var bigCats = ['tigers', 'lions', 'cheetah'];
// var dogs = ['retriever', 'pit bull'];
// var animals = [bigCats, dogs];

// for(var i = 0; i < animals[0].length; i++){
//   console.log(animals[0][i]);
// }

// for(var i = 0; i < animals[1].length; i++){
//   console.log(animals[1][i]);
// }


/* While Loops */
// var x = 0;

// while(x < 10){
// 	console.log(x);
// 	x++;
// }


/* Exercise - while loops. Simple decrementing loop */
// var beer = 99;

// while (beer > 0) {
// 	console.log(beer + ' bottles of beer on the wall');
// 	beer--;
// }

/* A bit more robust 99 Bottles of Beer */

// var beer = 99;
// while (beer > 0) {
// 	if (beer === 1) {
// 		var bottleFormatted = " bottle";
// 	} else {
// 		var bottleFormatted = " bottles";
// 	}

// 	var verse = [
// 		beer + bottleFormatted + " of beer on the wall,",
// 		beer + bottleFormatted +  " of beer!",
// 		"Take one down, pass it around",  
// 		(beer - 1) + " bottles of beer on the wall!"
// 	].join("\n");

// 	console.log(verse);

// 	beer--;
// }



/* Practical use of updating DOM with results of a for loop */
// $(document).on('ready', function(){

// 	var beers = ["lagers", "pale ale", "third beer", "fourth beer"];

// 	console.log(beers);

// 	for (var i = 0; i < beers.length; i++){
// 		var curBeer = beers[i];
// 		console.log('curBeer: ' + curBeer);
// 		$('#list-beers').append('<li>' + curBeer + '</li>');
// 	}

// });