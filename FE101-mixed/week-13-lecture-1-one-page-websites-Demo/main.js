$(document).on('ready', function(){

	$('body').css('margin-top', $('.nav-wrapper').outerHeight());

	$('.nav-main a').on('click', function(){
		var page = $(this).attr('href');
		var offset = $('.nav-wrapper').outerHeight();
		console.log(offset);
		
		$.scrollTo(page, {
			duration: 800,
			offset: -offset
		});

		// add current class
		$('.nav-main a').removeClass('is-current');
		$(this).addClass('is-current');
	});

	// $('.nav-main a').on('click', function(){
	// 	var href = $(this).attr('href');
	// 	$.scrollTo(href, 800);
	// });
	
	// $(window).on('scroll', function(){
	// 	var winTop = $(window).scrollTop();
	// 	console.log(winTop);

	// 	if(winTop > 500 && $('.nav-wrapper').css('position') !== 'fixed'){
	// 		$('.nav-wrapper').hide().css('position', 'fixed').slideDown().addClass('is-fixed');
	// 	} else if (winTop < 500 && $('.nav-wrapper').hasClass('is-fixed')){
	// 		$('.nav-wrapper').removeClass('is-fixed').slideUp(function(){
	// 			$('.nav-wrapper').css('position', 'static').show();
	// 		});
	// 	}

	// });

}); // Closes doc.ready
