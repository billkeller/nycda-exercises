

$(document).on('ready', function(){
	
	var buster = function(){
		alert('Im the callback, alerting');
	}
	
	
	var addNum = function(buster, num1, num2){
		console.log(num1 + num2);
		buster();
	}

	// addNum(buster, 1, 2);

	// store your callback in an anon function
	var foo = function(){
		console.log('logging here');
	}

	var bar = function(foo){
		// do your bar stuff
		console.log('doing my bar stuff');
		// invoke your calback
		foo();
	}

	// invoke bar, passing in foo, to be used as callback
	// bar(foo);
	
	$('.js-buster').on('click', function(){
		$('#buster').animate({
			height: '100px',
			width: '70%',
			opacity: .25
		}, 2000);
	});

	var animateMenu = function(){
		$('.menu').slideDown().animate({
			height: 500,
			width: '100%'
		})
	}
	// $('.menu').fadeIn(animateMenu);

	$('.js-link').on('click', function(){
		animateMenu();
		$('.js-link').trigger('buster');
	});

	$('.js-link').on('buster', function(){
		alert('custom event');
	});














	//$('.js-link').trigger('buster');
	$('.js-totally-custom').on('myCustomEvent', function(){
		console.log('World Domination');
	});

	// $('.js-totally-custom').trigger('myCustomEvent');





















	$('.js-menu-slide').on('click', function(){
		$('.menu').slideUp();
	})


}); // Closes document.on ready
