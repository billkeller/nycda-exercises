// var NYCDA = 'class';

var NYCDA = NYCDA || {};

NYCDA.utilities = {
	doMath: function(){
		alert('Doing math');
	},

	doNames: function(){
		alert('Doing names');
	},

	foo: 'bar'
}

NYCDA.users = {
	login: function(){

	},
	delete: function(){

	}
}















//object literal
var mycar = {
	brand: 'Prius',
	wheels: 4,
	saybrand: function(){
		alert('my car is a ' + this.brand)
	},
	'has-leather': false
}

// Constructor
var Car = function(brand, wheels){
	this.brand = brand;
	this.wheels = wheels;
	this.color = 'red';
}

// Adding a method via Cars prototype
Car.prototype.honk = function(){
	alert('honk honk');
}

Car.prototype.color = 'green';


var dadscar = new Car('toyota', 3);
dadscar.color = 'blue';
dadscar.moonroof = true;

var momscar = new Car('suzuki', 6);






var User = function(fname, lname){
	this.fname = fname;
	this.lname = lname;
}

// create a user:
var steve = new User('steve', 'stevenson');

var Admin = function(fname, lname){
	this.isAdmin = true;
	User.call(this, fname, lname);
}

//create and admin:
var james = new Admin('james', 'marvin')

var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'goat';
var steve = 'billy goat';















// named function
function carTalk(){
	alert('honk honk');
}

var love = 'coding';



























var myCar = {
  brand: 'Prius'
}

var love = 'coding';

function carTalk(){
	console.log('hello');
}




/* ------------------------------------------------
Slide 5: Object Literals
------------------------------------------------ */
// var my_car = {
// 	brand: 'prius',
// 	wheels: 4,
// 	friendly_brand: function(){
// 		alert(this.brand);
// 	}
// }

// var dolphin = {
// 	environment: 'water',
// 	eats: 'fish',
// 	fins: 1
// }


/* ------------------------------------------------
Slide 6: Exercise: Object Literal
------------------------------------------------ */
// var person = {
// 	name: "Steve",
// 	age: 25,
// 	dob: "Jan. 9, 1978" 
// }


/* -----------------------------------------------------
Slide 7-10: Constructor function
Constructor functions use an = sign, because we're setting vars
---------------------------------------------------------- */
// function Car(brand, wheels){
// 	this.brand = brand;
// 	this.wheels = wheels;
// }

/* Creating new INSTANCEs of Car */
// var my_car = new Car("ford", 3);
// var steves_car = new Car("Suzuki", 5);

/* Difference between objects and their instances */
/* A 'Car' has a brand, and wheels
	but an INSTANCE of a 'Car' (steves_car)
   has a brand of "suzuki" and a wheels count of 5 */
// console.log(steves_car);



/* ------------------------------------------------
Slide 11: Exercise: new constructor function
------------------------------------------------ */
// function Animal(species, firstName, sound) {
// 	this.species = species;
// 	this.firstName = firstName;
// 	this.sound = sound;
// }

/* two instances of an Animal object */
// var my_sheep = new Animal("sheep", "Lila", "baaaa");
// var my_dog = new Animal("dog", "pumpkin", "woof");



/* ------------------------------------------------
Slide 12-14: Reading Attributes
------------------------------------------------ */
// var my_car = {
// 	brand: "Prius",
// 	"has-leather": false
// }

// // my_car['brand']
// // my_car.brand
// // my_car['has-leather']
// // my_car.has-leather //THIS WILL NOT WORK


/* ------------------------------------------------
Slide 15: Storing functions in objects (Methods)
------------------------------------------------ */
// var my_car = {
// 	brand: "Prius",
// 	friendly_brand: function(){
// 		return "Your cars brand is " + this.brand; 
// 	}
// }

/* access the method */
// console.log(my_car.friendly_brand());



/* ------------------------------------------------
Slide 16: Exercise: Define an object using a constructor function
------------------------------------------------ */
// function Animal(species, sound) {
// 	this.species = species,
// 	this.sound = sound,
// 	this.speak = function(){
// 		return "A " + this.species + " says " + this.sound;
// 	}
// };

// var my_animal = new Animal("dog", "woof");







/* ------------------------------------------------
Slide 19: Prototypes: Sample Code
------------------------------------------------ */
// function Car(brand, wheels){
// 	this.brand = brand;
// 	this.wheels = wheels;
// }

/* Add a method to the parent object */
// Car.prototype.friendly_brand = function(){
//   return "This car's brand is " + this.brand;
// }

// var my_car = new Car("toyota", 4);

// console.log(my_car.friendly_brand());








/* ------------------------------------------------
Slide 20-21: Code example
------------------------------------------------ */
// function Car(brand, wheels){
// 	this.brand = brand;
// 	this.wheels = wheels;
// }

/* Add an attribute to the parent object */
// Car.prototype.color = "green"

// var my_car = new Car("toyota", 4);
// var my_other_car = new Car("ford", 4);

/* If the color attribute already existed, adding it via Car.prototype.color
would not work. You can however, modify your instance */
// my_car.color = "red";





/* ------------------------------------------------
Slide 22: Exercise
------------------------------------------------ */
// function Car(brand, wheels){
// 	this.brand = brand;
// 	this.wheels = wheels;
// 	this.friendly_brand = function(){
// 		return "This car's brand is " + this.brand;
// 	}
// }

// my_car = new Car("Lexus", 4);

// console.log(my_car);

// Car.prototype.friendly_wheels = function(){
// 	return "this car has " + this.wheels + " wheels.";
// }

// my_new_car = new Car("prius", 5);

// console.log(my_new_car);






/* ------------------------------------------------
Slide 24: Protypical Objects
------------------------------------------------ */
// function User(fname, lname, age){
// 	this.fname = fname;
// 	this.lname = lname;
// 	this.age = age;
// 	this.name = function(){
// 		return this.fname + " " + this.lname;
// 	}
// }

// function Admin(fname, lname, age){
// 	this.admin = true;
// 	User.call(this, fname, lname, age);
// }

// var mark = new User("Mark", "Markson", 30);
// var steve = new Admin("Steve", "Stevenson", 20);


/* ------------------------------------------------
Slide 25: Exercise: Protypical Objects
------------------------------------------------ */
// function Animal(species){
// 	this.species = species;
// }

// function Mammal(species){
// 	this.mammal = true;
// 	Animal.call(this, species);
// }

// var bird = new Animal("bird");
// var dolphin = new Mammal("dolphin");

/* Another example */
// function House(style, backyard){
// 	this.style = style;
// 	this.backyard = backyard;
// }

// function Room(style, backyard, roomCount){
// 	this.roomCount = roomCount;
// 	House.call(this, style, backyard);
// }

// var myHouse = new House('gigantic', true);
// var livingRoom = new Room('open floor', false, 5);