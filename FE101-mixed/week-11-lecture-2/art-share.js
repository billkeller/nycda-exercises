// return all users
var returnUsers = function(){
	$.ajax({
		type: "GET",
		url: "http://art-share.herokuapp.com/api/v1/users/"
	}).success(function(data){
		var user = data.result;
		console.log('all users: ', user);
	}).error(function(error){
		console.log('error msg: ', error);
	});
};

// note: this user has an ID of 10
var loginUser = function(){
	$.ajax({
		type: "POST",
		url: "http://art-share.herokuapp.com/api/v1/sessions/new",
		data: {
			email: "steve@steve.com",
			password: "passw0rd"
		}
	}).success(function(data){
		console.log("successful login. Here's the user:", data.result);
	});
};

// Get currently Logged in user
var getCurrentUser = function(){
	$.ajax({
		type: "GET",
		url: "http://art-share.herokuapp.com/api/v1/sessions/"
	}).success(function(response){
		console.log('success: ', response);
	});	
};

// $.ajax({
//   type: "GET",
//   url: "/api/v1/users/"
// }).success(function(response){
// 	console.log('response: ', response);
// })

// List all paintings for a user (user 10)
var listPaintings = function(userId){
	$.ajax({
		type: "GET",
		url: "http://art-share.herokuapp.com/api/v1/users/1/paintings/"
	}).success(function(response){
		for (i = 0; i < response.result.length; i++){
			if (response.result[i].user_id == userId){
				// console.log('foo');
				$('.images').append('<img src="' + response.result[i].image_url + '"><p>'+ response.result[i].name +'</p>')
				console.log(response.result[i]);

			}
		}
		
	}).error(function(response){
		console.log('error: ', response);
	});
};

// Create a painting (for user 10)
var addPainting = function(){
	$.ajax({
		type: "POST",
		url: "http://art-share.herokuapp.com/api/v1/users/10/paintings/",
		data:{
			painting: {
				image_url: "http://img.yessy.com/1646528113-28236b.jpg",
				name: "My fourth painting"
			}
		}
	}).success(function(response){
		console.log('Added a painting!: ', response.result);
	}).error(function(error){
		console.log('Error: ', error);
	});
}

// Delete a users painting
var deletePainting = function(paintingId){
	$.ajax({
		type: "DELETE",
		url: "http://art-share.herokuapp.com/api/v1/users/10/paintings/" + paintingId
	}).success(function(response){
		console.log('Deleted a painting: ', response)
	})
}

// Delete a user
var deleteUser = function(){
	$.ajax({
		type: "DELETE",
		url: "http://art-share.herokuapp.com/api/v1/users/10"
	}).success(function(response){
		console.log('Deleted a user: ', response)
	})
}




