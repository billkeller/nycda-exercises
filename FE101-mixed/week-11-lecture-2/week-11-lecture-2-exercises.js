var MS = {}

$(document).on('ready', function(){
	/* on submit */
	$("form").on('submit', function(){
		if($("#username").val() == ""){
			alert('Username cannot be blank');
			return false;
		} else if($("#password").val() == ""){
			alert('Password cannot be blank');
			return false;
		} else {
			alert('all clear');
		}
	});

	$('.snake').data('snake-type', 'python');

	/* Comparing by data data-attribute */
	// first = $("audio source").first();
	// last = $("audio source").last();

	// if(first.data("duration") < last.data("duration")){
	//   console.log("first track duration is longest");
	// } else {
	// 	console.log("second track duration is longest")
	// }

	/* Password, on Change */
	// $('#password').on('change', function(){
	// 	if ($('#password').val() === ''){
	// 		alert('please enter a password');
	// 	}
	// });

	// Game data
	currentHealth = 100;
	$('.player').data('health', currentHealth);
	$('.player-health').html(currentHealth);

	attack = function(){
		newHealth = currentHealth - 1;
		$('.player').data('health', newHealth);
		$('.player-health').html(newHealth);		
		currentHealth = newHealth;
		console.log('attack!')
	}

});