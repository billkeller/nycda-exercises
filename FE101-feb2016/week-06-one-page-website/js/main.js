$(document).on('ready', function(){
	var navHeight = $('.nav-wrapper').height();
	console.log(navHeight);

	$('body').css('margin-top', navHeight);

	$('.nav-wrapper a').on('click', function(e){
		e.preventDefault();
		var currentHref = $(this).attr('href');
		console.log(currentHref);
		// console.log($(this));
		$.scrollTo(currentHref, {
			duration: 800,
			offset: -navHeight
		});
	});
});