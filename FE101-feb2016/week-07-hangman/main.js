var HANG = HANG || {};

$(document).on('ready', function(){
	
	
	var hangMan = function(word){
		// setup our alphabet
		guesses = word.length;
		var alpha = 'abcdefghijklmnopqrstuvwxyz';
		var alphaArr = alpha.split('');

		for (var i = 0; i < alphaArr.length; i++){
			$('#js-letter-picker').append('<span data-letter="' + alphaArr[i] + '">' + alphaArr[i] + '</span>');
		}

		// modify hangMan to accept a string as an argument.
		// This string will be the word we want the user to guess
		
		// setup our js-word empty blocks
		// nearly the same way we setup the alphabet, above
		var wordArr = word.split('');
		for (var j = 0; j < wordArr.length; j++){
			$('#js-word').append('<span data-letter="' + wordArr[j] + '"></span>');
		}

		// onclick of a #js-letter-picker span, get it's data-letter
		// and then use a jquery attribute selector to select the corresponding data-letter
		// in our word
		$('#js-letter-picker span').on('click', function(){
			var selectedLetter = $(this).data('letter');
			// alert(selectedLetter);
			var count = $('#js-word span[data-letter="' + selectedLetter + '"]').length;
			console.log(count);
			if (count >= 1){
				// highlight the alphabet letter to show user they found one
			} else {
				// highlight the alphabet letter to show user this letter was NOT found
				guesses = guesses -1;
				console.log(guesses);
				if (guesses === 0){
					alert('you lost');
				}
			}

			$('#js-word span[data-letter="' + selectedLetter + '"]').text(selectedLetter);

			// check for winner by seeing if there are any empty spans in our word
			var winner = $('#js-word span:empty').length;
			console.log('empty spans remaining: ' + winner);
			if (winner === 0){
				alert('you have won!');
			}

			// remove click even from this
			$(this).off();
		});

	};

	hangMan('chickens');

}); // End doc.ready