$(document).on('ready', function(){
	
	var currentHealth = 100;

	$('.player').data('health', currentHealth);

	// update DOM with this info
	$('.player').text(currentHealth);

	var attack = function(){
		var newhealth = currentHealth -10;
		$('.player').data('health', newhealth);
		$('.player').text(newhealth);
		// update global var
		currentHealth = newhealth;			
	};

	$('.js-attack').on('click', function(){
		attack();
	});

});