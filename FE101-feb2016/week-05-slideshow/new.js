$(document).on('ready', function(){
	$('img').eq(0).show();

	var currentSlideNum = 0;
	var slideCount = $('img').length;

	var doSlides = function(direction){

		// hide currentSlide
		$('img').eq(currentSlideNum).fadeOut();

		if(direction === 'next'){
			// Increase currentSlideNum by 1, then show it
			currentSlideNum = currentSlideNum + 1;

			// if we're at the end, reset to zero
			if(currentSlideNum === slideCount){
				currentSlideNum = 0;
			}
		} else {
			if (currentSlideNum === 0){
				currentSlideNum = slideCount - 1;
			} else {
				currentSlideNum = currentSlideNum - 1;
			}
		}

		$('img').eq(currentSlideNum).fadeIn();

		console.log(currentSlideNum);
	};

	$('img').on('click', function(){
		doSlides('reverse');
	});

	$('.js-autoplay').on('click', function(){
		foo = setInterval(function(){
			doSlides('next');	
		}, 3000)
		
	});

});