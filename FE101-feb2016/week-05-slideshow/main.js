$(document).on('ready', function(){
	$('img').eq(0).show();

	// setup some defaults
	var currentSlideNum = 0;
	var slideCount = $('img').length;

	var doSlides = function(){
		// hide current img
		$('img').eq(currentSlideNum).fadeOut();
		// increase currentSlideNum by 1, and show it
		currentSlideNum = currentSlideNum + 1;
		
		if (currentSlideNum === slideCount) {
			// once we found the last slide, reset it to 0
			currentSlideNum = 0;
		}
		$('img').eq(currentSlideNum).fadeIn();

		console.group('Slide Info');
			console.log('currentSlideNum: ' + currentSlideNum);
			console.log('slideCount:' + slideCount);
		console.groupEnd();
	}

	$('img, .js-next').on('click', function(){
		doSlides();
	});

	$('.js-autoplay').on('click', function(){

	});
});

var foo = setInterval(function(){
	console.log('foo')	
}, 1000);

