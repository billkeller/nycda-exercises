
$(document).on('ready', function(){
	$(window).setBreakpoints({
		distinct: true,
		breakpoints: [200, 680]
	});

	var lrgImages = function(){
		var allImages = $('.js-img');
		
		// Regular for loop
		// for(var i = 0; i < allImages.length; i++){
		// 	var current = allImages[i];
		// 	var newSrc = $(allImages[i]).data('large');
		// 	$(allImages[i]).attr('src', newSrc);
		// 	console.log(newSrc);
		// }

		// jQuery .each() helper function
		$('.js-img').each(function(){
			var newSrc = $(this).data('large');
			$(this).attr('src', newSrc);
			console.log(newSrc);
		});
	}

	var smallImages = function(){
		var allImages = $('.js-img');

		$('.js-img').each(function(){
			var newSrc = $(this).data('small');
			$(this).attr('src', newSrc);
		})
	}

	var largeImg = $('.js-img').data('large');
	var smallImg = $('.js-img').data('small');
	
	$(window).bind('enterBreakpoint200', function(){
		// $('.js-img').attr('src', smallImg);
		smallImages();
	});

	$(window).bind('enterBreakpoint680', function(){
		//$('.js-img').attr('src', largeImg);
		lrgImages()
	});

	$(window).bind('exitBreakpoint680', function(){
		// $('.js-img').attr('src', smallImg);
		smallImages();
	});

	


});
